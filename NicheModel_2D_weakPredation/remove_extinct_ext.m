% Function remove_extinct removed species that have gone extinct from the
% abundance matrix A and from the model used to generate this matrix.
%
% Input
% A - Abundance matrix
% model - Simulation model
%
% Output
% A - Abundance matrix without the extinct species
% model - Simulation model without the extinct species
%
%
% changelog (AA)
%   15.02.2011   the entries in model.webTopology for extinct
%   species are no also removed
%

function [A, model, extinct_no] = remove_extinct_ext(A, model)

check = sum(A, 2);

extinct = find(check == 0);

A(extinct, :) = [];

model.speciesInteractionStrengths(extinct, :) = [];
model.speciesInteractionStrengths(:, extinct) = [];

model.webTopology(extinct, :) = [];
model.webTopology(:, extinct) = [];

model.speciesGrowthRates(extinct, :) = [];


extinct_no = size(extinct, 1);