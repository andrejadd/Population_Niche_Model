% Script to look at the species community dynamics
% This version looks at a two dimensional environment
% This version also makes no changes to relative competitive ability

% Last edited 2/10/05  J. Yearsley
% last edited 12/10  A.Aderhold
function [model, X, extinct, failure] = generateSimData_1D(id, ...
						  densityLimit, num_species, loc_dim, steps, connectance, pos_interactions, neg_interactions, rate, gamma, noise, debuglvl1)

%
%
% AA, e.g. run
% generateSimData_1D(35,10,100,1000,0.1,0.1,0.1,5)
%
%

%
%
% turn this to false in order to avoid most messages
%
if(exist('debuglvl1') == 0)
  debuglvl1 = false;
end


rand('state',sum(100*clock))
randn('state',sum(100*clock))


%% Parameter settings
% Simulation settings
% Set the random seeds for each random part of the model,
% zero means pick a truely random number
sim.RandomSeed.niche = 0; %10;
sim.RandomSeed.growthRate = 0; %[10,30,6];
sim.RandomSeed.intraDD = 0; %5;
sim.RandomSeed.interactionStrengths = 0; %[13,22];
sim.RandomSeed.initialAbundance = 0;

sim.filename.model = 'notThere'; % File to load starting values for model if it exists
sim.filename.initialX = 'notThere'; % File to load starting initial abundance values

sim.repeat = 1; % Number of repeats
sim.iter = steps;  % Number of iterations
sim.display = 0;  % If set then display the results
sim.debug = 0;
sim.dt = 0.001; % Time step
sim.figureNum = 2;
sim.displayInterval = 1;
sim.movie = 0; % If true then create a movie of the simulation
sim.displayAmax = 3;
% displayCode = 0   Plot species richness
% displayCode = -1   Plot mean abundance
% displayCode = -2  Plot identity of the most abundant
% displayCode = -4  Plot identity of the most abundant
% displayCode = -3  Plot an abundance transect through the data
% displayCode = i   Plot abundance of species i
sim.displayCode = 10;

sim.changeGrowthRate = 0;
sim.changeInteractions = 0;

% Community settings
com.nSites= loc_dim; % Number of spatial locations
com.nSpecies = num_species; % Number of species


if(exist('connectance') == 1)
  com.connectance = connectance;
else
  com.connectance = 0.1; % The connectance of the whole-web (assuming all species present)
end


com.Xmin = 0.01;  % Expected abundance required for species survival (exponential distribution)


if(exist('noise') == 1)
  com.e_sigma = noise;
else
  com.e_sigma = 0; % Variance of the environmental variation
end


if(exist('pos_interactions') == 1)
  com.interaction.PosMean = pos_interactions;
else 
  com.interaction.PosMean = 0.1;
end

if(exist('neg_interactions') == 1)
  com.interaction.NegMean = neg_interactions;
else 
  com.interaction.NegMean = 10;
end


com.interaction.PosSigma2 = 0.5;
com.interaction.NegSigma2 = 0.5;


if(exist('gamma') == 1)
  com.gamMax = gamma; % Max strength of intra-specific Gompertz density-dependence
  com.gamMin = gamma; % Min strength of intra-specific Gompertz density-dependence
 else 
  com.gamMax = 20;
  com.gamMin = 20;
end

%com.r_mean = 2; % Mean value of growth rate

if(exist('rate') == 1)
  com.rMax = rate;
else 
  com.rMax = 15; % Variation in growth rate
end


com.rMin = -com.rMax; % Variation in growth rate
com.rSigma = 0.1;
com.nResources = 1; % number of envrionmental variables that contribute to r

com.m = 0.1; % Mean distance of dispersal

% Initial conditions
com.nSpecies_mean = num_species/2;  % Mean number of species per site
com.X_Init = 3;

% show all the parameters
%disp(com)



% Call script which will do the simulation
    [X, deltaX, model, movieObj, iters] = communitySimulation_1d(sim,com,debuglvl1);

[X, model, extinct] = remove_extinct(full(X), model);




  % AA:  check if this was a valid run, only save model if valid
  %      Valid means no species died out or has densities that exceed certain limit 
  if iters ~= sim.iter,
%     disp('premature quit, skipping model..');
     failure=true;
     return;
  end

  %% check for death species
   if sum(sum(X == 0)) > 0,
     if(debuglvl1) 
       disp('death species!')
     end
     failure=true;
     return;
  end
    

  % check if there exist species exceeding the density Limit
  if sum(sum(X > densityLimit)) > 0,
    disp('exceeding density limit!')
     % AA: set the return parameter if we came until here
     failure = true;
     return;
  end


failure=false;

% AA: save the sparse matrix with edge infos (the web)
  [i,j] = find(model.webTopology);
  saveweb = [i,j];

% file containing the network
fileNetwork =  sprintf('Models/Model_id%i_type1_%d_%d_%.2f_%.1f_%.1f_%d_%d_net.txt', id, com.nSites, com.nSpecies, com.connectance, com.interaction.PosMean, com.interaction.NegMean, com.rMax, com.gamMax);

  save(fileNetwork, 'saveweb', '-ascii');

% and this is the actual species value for each location matrix [species x location]
  fileYvalues =  sprintf('Models/Model_id%i_type1_%d_%d_%.2f_%.1f_%.1f_%d_%d_data.txt', id, com.nSites, com.nSpecies,  com.connectance, com.interaction.PosMean, com.interaction.NegMean, com.rMax, com.gamMax);

  dlmwrite(fileYvalues, X, ' ');


%extinct = 0;
