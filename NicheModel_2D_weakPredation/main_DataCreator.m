


% init random number generator based on clocks
rng(sum(clock), 'twister')


data_id = 1;             % start with this id for the data
max_data_instances = 10; % number of data instances to create 

maxtrials = 2000;    % this is how long we retry to create new data 

loc_dim = [25 25];   % size of location grid
num_species = 10;    % number of species

steps = 2500;        % number of time points the population is simulated
conn = 0.1;          % connectivity factor 
posInt = 4;          % positive interaction factor
negInt = 8;          % negative interaction factor
spatBeta = -1;       % spatial beta
initialDensity = 3;
rMin = 8;
rMax = 12;

success = 0; 

for i=1:maxtrials,


    [model, X, extinct, failure] = generateSimData_2d(data_id, num_species, loc_dim, steps,  conn, posInt, negInt, 12, 0.5, rMin, rMax, spatBeta, initialDensity);


    % if there was no failure, increment data id and record successful data creation
    if ~failure,

      data_id = data_id + 1;
      success = success + 1;

      disp('success');

      % check if all models
      if success == max_data_instances,
        break;
      end
    end      
end






