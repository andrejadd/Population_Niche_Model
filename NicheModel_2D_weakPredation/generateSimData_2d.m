% Script to look at the species community dynamics
% This version looks at a two dimensional environment
% This version also makes no changes to relative competitive ability

% Last edited 2/10/05  J. Yearsley
function [model, A, extinct,failure] = generateSimData_2d(id, num_species, loc_dim, steps, connectance, pos_interactions, neg_interactions, gamma, noise, rMin, rMax, beta, initialDensity)

   

%% AA
densityLimit = 50;
minSpecies = 10;
maxSpecies = 10;
maxDeathSpeciesAtLocs = 50; 
debuglvl1 = true;




%% Parameter settings
% Simulation settings
% Set the random seeds for each random part of the model,
% zero means pick a truely random number
sim.RandomSeed.niche = 0; %10;
sim.RandomSeed.growthRate = 0; %[10,30,6];
sim.RandomSeed.intraDD = 0; %5;
sim.RandomSeed.interactionStrengths = 0; %[13,22];
sim.RandomSeed.initialAbundance = 0;

sim.filename.model = 'notThere'; % File to load starting values for model if it exists
sim.filename.DispersalMatrix = 'dispersalMatrix10x10'; % File to load starting values for model if it exists
sim.filename.initialA = 'trial31x31_Results_25-Oct-2005#1'; % File to load starting initial abundance values

sim.repeat = 1; % Number of repeats
sim.iter = steps;  % Number of iterations
sim.display = 0;  % If set then display the results
sim.debug = 1;
sim.dt = 0.01; % Time step
sim.figureNum = 2;
sim.displayInterval = 1000;
sim.movie = 0; % If true then create a movie of the simulation
sim.displayAmax = 3;
% displayCode = 0   Plot species richness
% displayCode = -1   Plot mean abundance
% displayCode = -2  Plot identity of the most abundant
% displayCode = -4  Plot identity of the most abundant
% displayCode = -3  Plot an abundance transect through the data
% displayCode = i   Plot abundance of species i
sim.displayCode = 10;

sim.changeGrowthRate = 0;
sim.changeInteractions = 0;

% Community settings
com.nSites= loc_dim; % Number of spatial locations
com.nSpecies = num_species; % Number of species
if(nargin < 5)
  com.connectance = 0.1; % The connectance of the whole-web (assuming all species present)
else
  com.connectance = connectance;
end
com.Amin = 0.05;  % Expected abundance required for species survival (exponential distribution)


if(nargin < 9)
  com.e_sigma = 0.5; % Variance of the environmental variation
else
  com.e_sigma = noise;
end
% Good parameters 0, 10 with gam 8, growth rate 10, 0, connectance 0.1
if(nargin < 6)
  com.interaction.PosMean = 4;
else
  com.interaction.PosMean = pos_interactions;
end

if(nargin < 7)
  com.interaction.NegMean = 8;
else
  com.interaction.NegMean = neg_interactions;
end

com.interaction.PosSigma2 = 0.5;
com.interaction.NegSigma2 = 0.5;
if(nargin < 8)
  com.gamMax = 12;
  com.gamMin = 12;
else
  com.gamMax = gamma; % Max strength of intra-specific Gompertz density-dependence
  com.gamMin = gamma; % Min strength of intra-specific Gompertz density-dependence
end

if(nargin < 10)
  com.rMin = 0; 
else
  com.rMin = rMin; % Variation in growth rate
end

if(nargin < 11)
  com.rMax = 10; 
else
  com.rMax = rMax; 
end

com.nResources = 1; % number of envrionmental variables that contribute to r

if(nargin < 12)
  com.spatialBeta = -1;
else
  com.spatialBeta = beta;
end

com.m = 0.1; % Mean distance of dispersal

if(nargin < 12)
  com.A_Init = 3;
else
  com.A_Init = initialDensity;
end

% Initial conditions
com.nSpecies_mean = num_species/2;  % Mean number of species per site

% the fflush is for octave


disp( ['\n' 'id: ' num2str(id) ', PosMean: ' num2str(com.interaction.PosMean) ', NegMean: ' num2str(com.interaction.NegMean) ', rMin: ' num2str(com.rMin) ', rMax: ' num2str(com.rMax) ', spat.Beta: ' num2str(com.spatialBeta) ', initialDensity: ' num2str(com.A_Init) ] );
%fflush(stdout);


% Call script which will do the simulation
[A, model, movieObj, iters, tot_num_species] = communitySimulation_2d(sim,com,debuglvl1, minSpecies);

[A, model, extinct] = remove_extinct_ext(full(A), model);

deathlocs = sum(sum(A == 0));
species_left = (num_species - extinct);

disp(['left species: ' num2str(species_left) ', death locations: ' num2str(deathlocs)])

% AA:  check if run was not quit premature (happens when error
% occurs such as too many species gone extinct
if iters ~= sim.iter,
  disp('dropping run: too many died');
  failure=true;
  return;
end


% check if species left are in the boundaries
if  species_left < minSpecies | species_left > maxSpecies,
  disp('dropping run: outside of minSpecies and maxSpecies boundaries')
  failure=true;
  return;
end

%% check for death species
if sum(sum(A == 0)) > maxDeathSpeciesAtLocs,
   if(debuglvl1) 
     disp('dropping run: too much death locations')
%      disp(sum(sum(A==0) > 0))
   end
   failure=true;
   return;
end

    
% check if there exist species exceeding the density Limit
if sum(sum(A > densityLimit)) > 0,
  disp('exceeding density limit!')
   % AA: set the return parameter if we came until here
   failure = true;
   return;
end

%% AA: if we came until here, the run was a success
failure=false;

%% no sparse matrix to make it readable for R
interactions = full(model.speciesInteractionStrengths);

%% this creates the full network with logical values (1) for an
% edge because of the not equal zero operation  
network = full(model.speciesInteractionStrengths ~= 0);

disp('writing file');

% save to matlab file
fileOut =  sprintf('Models/Data_id%i.mat', id);
save(fileOut, 'interactions','network', 'A', '-v6')





