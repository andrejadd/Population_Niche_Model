

library("R.matlab")
 
filelist = list.files(".", "Data_id*")

description="Generated on 24.05.2011, octave, inverted sign, max. 50 death species on whole grid"

for (file in filelist) {

    cat("reading file ", file, "\n")

    dat = readMat(file)

    # nr of species
    nrspecies = dim(dat$A)[1]

    cat("nrspecies: ", nrspecies, "\n")

    Model = list(networks=NULL, CPlist=NULL, nrnodes=nrspecies, Ymatrix=dat$A, Alist=list(), Ematrix=NULL, Bmatrix=NULL, cycles=NULL, XE=NULL, YE=NULL, minPhase=NULL, HOMOGENEOUS_STRUCT=T, totallocs=625, xlocs=25,ylocs=25,Description=description)

    Model$networks[[1]] = dat$network
    
    cat("dim network: ")
    print.table(dim(dat$network))

    # get the id
    midtmp = unlist(strsplit(file, "_"))[2]
    midtmp = gsub(".mat", "", midtmp)
    mid = as.numeric(gsub("id", "", midtmp)) 



    fileout = paste("Model_id", mid, ".Rdata", sep="")
    save(Model, file=fileout)
}
