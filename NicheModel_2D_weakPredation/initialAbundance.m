% 
% Funtion: initialAbundance - the original, nothing changed
%
% Note: increasing the mean population density will increase the chance that a location is populated as long as numSp > comData.nSpecies
%
%   numSp = ceil(comData.nSpecies_mean*(1+randn(1)));
%
%   but numSp can become eventually anyways because of (1+randn(1)) and then the location is empty anyways.
%
% 
%
%


%% Initial Abundance
function initialA = initialAbundance(comData,simData)

if simData.RandomSeed.initialAbundance~=0,
    % Set random seed if required
    s = rand('state');
    rand('state',simData.RandomSeed.initialAbundance);
    randn('state', simData.RandomSeed.initialAbundance);
end


% AA, The ones function gives a [node x locations] matrix of ones
initialA = comData.A_Init * ones(comData.nSpecies,prod(comData.nSites));

% Keep those species who have gone extinct
ind = initialA < comData.Amin;
initialA(ind) = 0;

% And remove species so that the community size on average is
% comData.nSpecies_mean
for k=1:prod(comData.nSites),
    if mod(k, round(comData.nSites/100)) == 0
      fprintf('Abundances done %g\n', k/prod(comData.nSites))
    end

    % AA: numSp can become negative! 	
    numSp = ceil(comData.nSpecies_mean*(1+randn(1)));
    
    if numSp>comData.nSpecies,

      % AA: this leaves all species at this location at the mean density
      numSp=comData.nSpecies;
    elseif numSp<0,
      % AA: in this case the location becomes empty
      numSp = 0;
    end
    rand_sp = randperm(comData.nSpecies);

    % AA: set the densities for location k to zero 
    initialA(rand_sp(1:(comData.nSpecies-numSp)),k)= 0;
end

% % This just puts all species in the centre of the environment,
% % as a test of the spatial dispersal code
% initialA = zeros(size(initialA));
% ind = (comData.nSites(2)+1) * (round(comData.nSites(1)/2)-1)+1;
% initialA(:,ind) = 3*ones(comData.nSpecies,1);

% % This just puts all species at one random location,
% % as a test of the spatial dispersal code
% initialA = zeros(size(initialA));
% for i=1:comData.nSpecies,
%     ind = 1 + floor(rand*prod(comData.nSites));
%     initialA(i,ind) = 3;
% end

% Make A and c sparse matrices
initialA = sparse(initialA);

return
