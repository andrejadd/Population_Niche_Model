%% Growth rate function
function [r,resources] = growthRates(comData,simData,R),

% R is the resource coordinate
% Make sure it is a row vector
R = reshape(R,1,length(R));

if any(simData.RandomSeed.growthRate~=0),
    % Set random seed if required
    s = rand('state');
    sn = randn('state');
    rand('state',simData.RandomSeed.growthRate(1));
    randn('state',simData.RandomSeed.growthRate(2:3)');
end


% Method of creating species growth rates
% Method = 1  random, independent of space
% Method = 2  product of gaussians
% Method = 3  sum of linear contributions
% Method 4 use 1/f noise
method = 4;

if method==1,
    % Growth rates for each species independent of spatial position
    r = comData.r_mean * abs(1+sqrt(comData.r_cv)*repmat(randn(comData.nSpecies,1),1,comData.nSites));
    % Rescale the growth rates
    r = r / mean(nonzeros(r));
elseif method==2,
    % For each resource define the position at which optimum resource occurs
    optimalX =  0.5*sqrt(comData.nResources)*randn(comData.nSpecies,comData.nResources);
    % weighting of each resource to the growth rate
    weights = 10*rand(comData.nSpecies,comData.nResources);


    % This lets growth rate be the product of N Gaussian distributions
    r = exp(- (sum(weights,2) * X.^2 ...
        - 2*sum(weights.*optimalX,2) * X + ...
        sum(weights.*optimalX.^2,2)*ones(size(X)))/comData.nResources);
    r_mean = mean(r,2);
    r = comData.r_mean * r ./ r_mean(:,ones(1,comData.nSites));
elseif method==3
    % Let the growth rate be a sum of linear contributions
    % First assumes that growth rate must be positive
    r_x0 = mean(max([0 comData.rMin]) + (comData.rMax - max([0 comData.rMin])) * rand(comData.nSpecies,comData.nResources),2);
    % Second assumes it can have the full range
    r_x1 = mean(comData.rMin + (comData.rMax - comData.rMin) * rand(comData.nSpecies,comData.nResources),2);

    % Place positive growth rate on left (X=1)
    resources.r_x0 = r_x0;
    resources.r_x1 = r_x1;

    ind = rand(comData.nSpecies,1)<0.5;
    % Half the time place positive growth rate on right (X=max(X))
    resources.r_x0(ind,:) = r_x1(ind,:);
    resources.r_x1(ind,:) = r_x0(ind,:);

    r = resources.r_x1(:,ones(size(R))) + ...
        ( resources.r_x0 - resources.r_x1 ) * (R - min(R))/(max(R)-min(R));
elseif method == 4,
    %    for i=1:comData.nResources,
    %        resources(i,:) = reshape(spatialPattern(comData.nSites,-4),1,prod(comData.nSites));
    %    end
    resources = 0;
%     r(1,:) = reshape(spatialPattern(comData.nSites,comData.spatialBeta),1,prod(comData.nSites));
%     r(1,:) = comData.rMin + (comData.rMax-comData.rMin) * (r(1,:)-min(r(1,:))) / (max(r(1,:)) - min(r(1,:)));
    for i=1:comData.nSpecies,
%       r(i,:) = r(1, :);
%       r(i,:) = r(1, :);
        r(i,:) = reshape(spatialPattern(comData.nSites,comData.spatialBeta),1,prod(comData.nSites));
        r(i,:) = comData.rMin + (comData.rMax-comData.rMin) * (r(i,:)-min(r(i,:))) / (max(r(i,:)) - min(r(i,:)));
    end
end

if simData.RandomSeed.growthRate~=0,
    rand('state',s);
    randn('state',sn);
end

return
