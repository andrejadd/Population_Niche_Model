

function[] = batch_MassModelCreator(id, num_species,  beta, rMin, rMax, initialDensity)  

 conntrials = 0;

 maxtrials = 3000;
 trialsperParam = 30;

 loc_dim = [25 25];
 steps = 3000;
 conn = 0.1;
 posInt = 4;
 negInt = 8;

% disp( ['id: ' num2str(id) ', posInt: ' num2str(posInt) ' negInt ' num2str(negInt) ] );

success = 0;
conntrials = 0;

for i=1:maxtrials,


	[model, X, extinct, failure] = generateSimData_2d(id, num_species, loc_dim, steps,  conn, posInt, negInt, 12, 0.5, rMin, rMax, beta, initialDensity);
    
    %failure = false;
    % if there was no failure, try creating next model
    if ~failure,
      id = id + 1;
      success = success + 1;
      
      disp('success');
      
      conntrials = conntrials + 1;
      
      % check if all models
      if conntrials == trialsperParam,
	break;
	conntrials = 0;
      end
    end      
end

%successvector = [successvector; success];

%successvector      



