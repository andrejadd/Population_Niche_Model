%
%
%
% This gives all species the same starting density of comData.A_Init
%
%



%% Initial Abundance
function initialA = initialAbundanceUniform(comData,simData)

% AA, The ones function gives a [node x locations] matrix of ones
initialA = comData.A_Init * ones(comData.nSpecies,prod(comData.nSites));

% Make A and c sparse matrices
initialA = sparse(initialA);

return
