
This simulation software can be used to create species populations in two dimensional space. It contains two variants: strong and weak predation (see sub-folder), and both are self-contained. Use the batch_run*.m methods as a starting point.

This software is used the following publication:

```
Hierarchical Bayesian models in ecology: Reconstructing species interaction networks from non-homogeneous species abundance data: Andrej Aderhold, Dirk Husmeier, Jack J.Lennon, Colin M.Beale, and V. AnneSmith
```
